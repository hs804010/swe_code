import math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation


def Calculate_SWE_FBC(E, F, miu, dt, dx):
    dt = 0.01
    dx = 0.01 * math.pi
    X = 2 * math.pi
    T = 10
    C0 = 1 / 1.2074
    g = 10
    H = 1

    num_of_time = int(T / dt)
    num_of_space = int(X / dx) + 1

    u = np.zeros((num_of_time, num_of_space))
    h = np.zeros((num_of_time, num_of_space))
    ts = np.zeros(num_of_time)
    xs = np.zeros(num_of_space)

    'complete the calculation of t and x'
    for n in range(num_of_time):
        ts[n] = n * dt
    for i in range(num_of_space):
        xs[i] = i * dx

    'initial conditions'
    print(xs)
    for i in range(num_of_space):
        x = i * dx
        h[0, i] = C0 * (math.exp(-4 * (((x - math.pi) / 2) ** 2)) * math.sin(3 * ((x - math.pi) / 2)) + math.exp(
            -2 * ((x - math.pi) ** 2)) * math.sin(8 * (x - math.pi)))
    print(num_of_space - 1)

    for n in range(num_of_time - 1):

        for j in range(0, num_of_space):
            if j == 0:
                u[n + 1, 0] = u[n, 0] - dt * g * (h[n, 1] - h[n, -1]) / (2 * dx)
            elif j == num_of_space - 1:
                u[n + 1, j] = u[n, j] - dt * g * (h[n, 0] - h[n, j - 1]) / (2 * dx)
            else:
                u[n + 1, j] = u[n, j] - dt * g * (h[n, j + 1] - h[n, j - 1]) / (2 * dx)

        for j in range(0, num_of_space):
            if j == 0:
                h[n + 1, j] = h[n, j] - dt * H * (u[n + 1, j + 1] - u[n + 1, j - 1]) / (2 * dx)
            elif j == num_of_space - 1:
                h[n + 1, j] = h[n, j] - dt * H * (u[n + 1, 0] - u[n + 1, j - 1]) / (2 * dx)
            else:
                h[n + 1, j] = h[n, j] - dt * H * (u[n + 1, j + 1] - u[n + 1, j - 1]) / (2 * dx)

    return C0, xs, h, u


if __name__ == '__main__':
    C0, xs, h, u = Calculate_SWE_FBC(0.01, 1, 0.01, 0.01, 0.1 * math.pi)
    plt.plot(xs, h[-1])
    print(sum(h[0]))
    print(max(h[0]))
    print(C0)

    plt.show()
    fig, ax = plt.subplots()
    line, = ax.plot(xs, h[0])

    def animate(i):
        line.set_ydata(h[i])  # update the data.
        return line,
    ani = animation.FuncAnimation(
        fig, animate, interval=20, blit=True, save_count=50)

    plt.show()
